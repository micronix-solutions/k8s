# References:
# https://docs.docker.com/engine/examples/running_ssh_service/
FROM registry.gitlab.com/micronix-solutions/k8s/php-plus-for-wordpress:latest
RUN apt-get update && apt-get install openssh-server vim nano python3 git gnupg2 curl wget ruby-full mariadb-client -y
#RUN gpg2 --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB 
#RUN curl -sSL https://get.rvm.io | bash -s stable 
#RUN usermod -G rvm -a root
#RUN /bin/bash -c "source /etc/profile.d/rvm.sh"
#RUN /usr/local/rvm install 2.5.7
#RUN /usr/local/rvm --default use 2.5.7
RUN mkdir /var/run/sshd && mkdir /root/persistent
COPY authorized_keys /root/.ssh/authorized_keys
COPY entrypoint.sh /entrypoint.sh
RUN chmod 700 /root/.ssh/authorized_keys

EXPOSE 22
#CMD ["/usr/sbin/sshd", "-D", "-E", "/var/log/sshd", "-p", "22"]
ENTRYPOINT /entrypoint.sh
